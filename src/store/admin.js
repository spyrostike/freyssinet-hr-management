
import AdminProvider from '@/resources/admin-provider'
const adminService = new AdminProvider()

const admin = {
    namespaced: true,
    state: {
        token: JSON.parse(localStorage.getItem(process.env.VUE_APP_USER_TOKEN_NAME)) || null,
        status: null,
        isLoading: false,
        profile: {}
    },
    mutations: {
        SET_TOKEN: (state, payload) => {
            state.token = payload
        },
        GET_PROFILE: (state, payload) => {
            state.profile = payload
        }
    },
    actions: {
        AUTH_LOGIN: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                commit('app/SET_LOADING', true, { root: true })
                
                adminService.authLogin(data).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        localStorage.setItem(process.env.VUE_APP_USER_TOKEN_NAME, JSON.stringify(Data.Value.Token))
                        commit('SET_TOKEN', Data.Value.Token)
                        commit('app/SET_LOADING', false, { root: true })
                        resolve()
                    } else {
                        localStorage.removeItem(process.env.VUE_APP_USER_TOKEN_NAME)
                        commit('SET_TOKEN', null)
                        commit('app/SET_LOADING', false, { root: true })
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve()
                    }

                }).catch(error => {
                    console.log('error', error)
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('app/SET_LOADING', false, { root: true })
                    // localStorage.removeItem(process.env.VUE_APP_ADMIN_TOKEN_NAME)
                    reject()
                })
            })
        },
        AUTH_LOGOUT: ({commit, dispatch}) => {
            return new Promise((resolve, reject) => {
                commit('SET_TOKEN', null)
                localStorage.removeItem(process.env.VUE_APP_USER_TOKEN_NAME)
                // remove the axios default header
                // delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        }
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status
    }
}

export default admin