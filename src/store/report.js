// import { save, find, get } from '@/resources/employee-provider'
import ReportProvider from '@/resources/report-provider'
import i18n from '../i18n'
const reportService = new ReportProvider()

const admin = {
  namespaced: true,
  state: {
    criteria: {
      ContractTypeId: null,
      Id: null,
      DateFrom: null,
      DateTo: null,
      AutoCompleteText: null,
      PagingCriteria: {
        PageIndex: 0,
        PageSize: 10
      }
    },
    criteriaWithoutPaging: {
      ContractTypeId: null,
      Id: null,
      DateFrom: null,
      DateTo: null,
      AutoCompleteText: null,
    },
    criteriaDashboard: {
      ContractTypeId: null,
      Id: null,
      DateFrom: null,
      DateTo: null,
      AutoCompleteText: null,
    },
    item: null,
    ageRangeItems: [],
    yeasExperienceItems: [],
    turnOverRateItem: null,
    turnOverRateDetailItems: [],
    employeeItems: [],
    leaveHistoryTypeGroupingItems: [],
    leaveBalanceItems: [],
    trainingHistoryItems: [],
    trainingHistoryInfoItems: [],
    trainingAgeGroupingItems: [],
    trainingSummaryItems: [],
    totalRecords: 0,
    totalPages: 0,
    isLoading: false
  },
  mutations: {
    SET_CRITERIA: (state, payload) => {
      state.criteria = payload
    },
    SET_CRITERIA_WITHOUT_PAGING: (state, payload) => {
      state.criteriaWithoutPaging = payload
    },
    SET_CRITERIA_DASHBOARD: (state, payload) => {
      state.criteriaDashboard = payload
    },
    SET_ITEM: (state, payload) => {
      state.item = payload
    },
    SET_AGE_ITEMS: (state, payload) => {
      state.ageRangeItems = payload
    },
    SET_YEAR_EXPERIENCE_ITEMS: (state, payload) => {
      state.yeasExperienceItems = payload
    },
    SET_TURN_OVER_RATE_ITEM: (state, payload) => {
      state.turnOverRateItem = payload
    },
    SET_TURN_OVER_RATE_DETAIL_ITEMS: (state, payload) => {
      state.turnOverRateDetailItems = payload
    },
    SET_EMPLOYEE_ITEMS: (state, payload) => {
      state.employeeItems = payload
    },
    SET_LEAVE_HISTORY_TYPE_GROUPING: (state, payload) => {
      state.leaveHistoryTypeGroupingItems = payload
    },
    SET_LEAVE_BALANCE_ITEMS: (state, payload) => {
      state.leaveBalanceItems = payload
    },
    SET_TRAINING_HISTORY_ITEMS: (state, payload) => {
      state.trainingHistoryItems = payload
    },
    SET_TRAINING_HISTORY_INFO_ITEMS: (state, payload) => {
      state.trainingHistoryInfoItems = payload
    },
    SET_TRAINING_AGE_GROUPING: (state, payload) => {
      state.trainingAgeGroupingItems = payload
    },
    SET_TRAINING_SUMMARY: (state, payload) => {
      state.trainingSummaryItems = payload
    },
    SET_TOTAL_RECORDS: (state, payload) => {
      state.totalRecords = payload
    },
    SET_TOTAL_PAGES: (state, payload) => {
      state.totalPages = payload
    },
    SET_IS_LOADING: (state, payload) => {
      state.isLoading = payload
    }
  },
  actions: {

    //#region EmployeeReport
    AGE_RANGE: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.ageRange(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {

            let maleTotalAmount = Data.Value.Items.map(t => t.MaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)
            let femaleTotalAmount = Data.Value.Items.map(t => t.FemaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)
            let summary = maleTotalAmount + femaleTotalAmount

            let ageGroupingItems = [
              ...Data.Value.Items,
              { Grouping: i18n.t('report-summary'), MaleAmount: maleTotalAmount, FemaleAmount: femaleTotalAmount },
              { FemaleAmount: summary }
            ]
            // console.log("ageGroupingItems==>", ageGroupingItems);
            commit('SET_AGE_ITEMS', ageGroupingItems)
            commit('SET_TOTAL_PAGES', Data.Value.TotalPage)
            commit('SET_IS_LOADING', false)
            resolve(ageGroupingItems)
          } else {
            commit('SET_AGE_ITEMS', [])
            commit('SET_TOTAL_PAGES', 0)
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_AGE_ITEMS', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    AGE_RANGE_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.ageRangeExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    YEAR_EXPERIENCE: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.yearExperience(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {

            let maleTotalAmount = Data.Value.Items.map(t => t.MaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)
            let femaleTotalAmount = Data.Value.Items.map(t => t.FemaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)
            let summary = maleTotalAmount + femaleTotalAmount

            let yeasExperienceGroupingItems = [
              ...Data.Value.Items,
              { Grouping: i18n.t('report-summary'), MaleAmount: maleTotalAmount, FemaleAmount: femaleTotalAmount },
              { FemaleAmount: summary }
            ]

            commit('SET_YEAR_EXPERIENCE_ITEMS', yeasExperienceGroupingItems)
            commit('SET_TOTAL_PAGES', Data.Value.TotalPage)
            commit('SET_IS_LOADING', false)
            resolve(yeasExperienceGroupingItems)
          } else {
            commit('SET_YEAR_EXPERIENCE_ITEMS', [])
            commit('SET_TOTAL_PAGES', 0)
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_YEAR_EXPERIENCE_ITEMS', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    YEAR_EXPERIENCE_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.yearExperienceExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    TURN_OVER_RATE: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.turnOverRate(payload).then(v => {
          const { StatusCode, Errors, Data } = v
          console.log('TURN_OVER_RATE', v)
          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            console.log('data', v)

            let signSummaryMaleTypes = Data.Value.SignAgeGroupingItems.map(item => item.MaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)

            let signSummaryFemaleTypes = Data.Value.SignAgeGroupingItems.map(item => item.FemaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)

            let signSummary = signSummaryMaleTypes + signSummaryFemaleTypes

            let signAgeGroupingItems = [
              ...Data.Value.SignAgeGroupingItems,
              { Grouping: i18n.t('report-summary'), MaleAmount: signSummaryMaleTypes, FemaleAmount: signSummaryFemaleTypes },
              { FemaleAmount: signSummary }
            ]

            let resignSummaryMaleTypes = Data.Value.ResignAgeGroupingItems.map(item => item.MaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)

            let resignSummaryFemaleTypes = Data.Value.ResignAgeGroupingItems.map(item => item.FemaleAmount)
              .reduce((a, b) => {
                return a + b;
              }, 0)

            let resignSummary = resignSummaryMaleTypes + resignSummaryFemaleTypes

            let ResignAgeGroupingItems = [
              ...Data.Value.ResignAgeGroupingItems,
              { Grouping: i18n.t('report-summary'), MaleAmount: resignSummaryMaleTypes, FemaleAmount: resignSummaryFemaleTypes },
              { FemaleAmount: resignSummary }
            ]

            const item = { SignAgeGroupingItems: [...signAgeGroupingItems], ResignAgeGroupingItems: [...ResignAgeGroupingItems] }

            console.log('summaryMailTypes', ResignAgeGroupingItems)

            commit('SET_TURN_OVER_RATE_ITEM', item)
            commit('SET_IS_LOADING', false)
            resolve(item)
          } else {
            commit('SET_TURN_OVER_RATE_ITEM', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_TURN_OVER_RATE_ITEM', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    TRUN_OVER_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.turnOverExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    FIND_TURN_OVER_DETAIL: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        reportService.findTurnOverDetail(payload).then(v => {
          const { StatusCode, Errors, Data } = v
          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_TURN_OVER_RATE_DETAIL_ITEMS', Data.Value.Items)
            resolve(Data.Value)
          } else {
            commit('SET_TURN_OVER_RATE_DETAIL_ITEMS', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            reject(Errors[0].Message)
          }

        })
      }).catch(error => {
        commit('SET_TURN_OVER_RATE_DETAIL_ITEMS', [])
        commit('app/SET_ERROR_MESSAGE', error, { root: true })
        reject(error)
      })
    },
    EMPLOYEE: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.employee(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_EMPLOYEE_ITEMS', Data.Value.WorkingContractGroupingItems)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value)
          } else {
            commit('SET_EMPLOYEE_ITEMS', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_EMPLOYEE_ITEMS', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    EMPLOYEE_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.employeeExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    RESIGNATION_EMPLOYEE: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.resignationEmployee(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_EMPLOYEE_ITEMS', Data.Value.ResignContractGroupingItems)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value)
          } else {
            commit('SET_EMPLOYEE_ITEMS', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_EMPLOYEE_ITEMS', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    RESIGNATION_EMPLOYEE_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.resignationEmployeeExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    //#endregion EmployeeReport

    //#region LeaveReport
    LEAVE_HISTORY_TYPE_GROUPING: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.leaveHistoryTypeGrouping(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_LEAVE_HISTORY_TYPE_GROUPING', Data.Value.Items)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value)
          } else {
            commit('SET_LEAVE_HISTORY_TYPE_GROUPING', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_LEAVE_HISTORY_TYPE_GROUPING', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    LEAVE_HISTORY_TYPE_GROUPING_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.leaveHistoryTypeGroupingExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    LEAVE_BALANCE: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.leaveBalance(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_LEAVE_BALANCE_ITEMS', Data.Value.Items)
            commit('SET_TOTAL_PAGES', Data.Value.TotalPage)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value.Items)
          } else {
            commit('SET_LEAVE_BALANCE_ITEMS', [])
            commit('SET_TOTAL_PAGES', 0)
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_LEAVE_BALANCE_ITEMS', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    //#endregion LeaveReport

    //#region TriningReport
    TRAINING_HISTORY: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.trainingHistory(payload).then(v => {
          const { StatusCode, Errors, Data } = v
          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_TRAINING_HISTORY_ITEMS', Data.Value.Items)
            commit('SET_TOTAL_PAGES', Data.Value.TotalPage)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value)
          } else {
            commit('SET_TRAINING_HISTORY_ITEMS', [])
            commit('SET_TOTAL_PAGES', 0)
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_TRAINING_HISTORY_ITEMS', [])
          commit('SET_TOTAL_PAGES', 0)
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    TRAINING_HISTORY_INFO: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        reportService.trainingHistoryInfo(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_TRAINING_HISTORY_INFO_ITEMS', Data.Value)
            resolve(Data.Value)
          } else {
            commit('SET_TRAINING_HISTORY_INFO_ITEMS', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_TRAINING_HISTORY_INFO_ITEMS', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          reject(error)
        })
      })
    },
    TRAINING_AGE_GROUPING: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.trainingAgeGrouping(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_TRAINING_AGE_GROUPING', Data.Value)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value)
          } else {
            commit('SET_TRAINING_AGE_GROUPING', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_TRAINING_AGE_GROUPING', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    TRAINING_AGE_GROUPING_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.trainingAgeGroupingExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    TRAINING_SUMMARY: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        console.log('payload', payload)
        reportService.trainingSummary(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_TRAINING_SUMMARY', Data.Value)
            commit('SET_IS_LOADING', false)
            resolve(Data.Value)
          } else {
            commit('SET_TRAINING_SUMMARY', [])
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('SET_TRAINING_SUMMARY', [])
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    TRAINING_SUMMARY_EXPORT: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        commit('SET_IS_LOADING', true)
        reportService.trainingSummaryExport(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            commit('SET_IS_LOADING', false)
            resolve(Data)
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    }
    //#endregion TriningReport

  },
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status
  }
}

export default admin