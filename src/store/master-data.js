
// import { 
//     findProvince, 
//     findDistrict, 
//     findSubDistrict, 
//     findDepartment, 
//     findContractsType, 
//     findNationality, 
//     findPosition, 
//     getAllGender,
//     findLeaveType,
//     getAllLeaveStatus,
//     getAllTrainingCourseType } from '@/resources/master-data-provider'

import MasterDataProvider from '@/resources/master-data-provider'

const masterDataService = new MasterDataProvider()

const admin = {
    namespaced: true,
    state: {
        provinceItems: [],
        districtItems: [],
        subDistrictItems: [],
        departmentItems: [],
        contractsTypeItems: [],
        personInfoContractsTypeItems: [],
        nationalityItems: [],
        positionItems: [],
        genderItems: [],
        leaveTypeItems: [],
        leaveStatusItems: [],
        trainingCourseTypeItems: [],
        periodTypeItems: [],
        roleTypeItems: [],
        courseGroupItems: [],
        courseGroupItems: [],
        annuallyItems: []
    },
    mutations: {
        SET_PROVINCE_ITEMS: (state, payload) => {
            state.provinceItems = payload
        },
        SET_DISTRICT_ITEMS: (state, payload) => {
            state.districtItems = payload
        },
        SET_SUB_DISTRICT_ITEMS: (state, payload) => {
            state.subDistrictItems = payload
        },
        SET_DEPARTMENT_ITEMS: (state, payload) => {
            state.departmentItems = payload
        },
        SET_CONTRACTS_TYPE_ITEMS: (state, payload) => {
            state.contractsTypeItems = payload
        },
        SET_PERSON_CONTRACTS_TYPE_ITEMS: (state, payload) => {
            state.personInfoContractsTypeItems = payload
        },
        SET_NATIONALITY_ITEMS: (state, payload) => {
            state.nationalityItems = payload
        },
        SET_POSITION_ITEMS: (state, payload) => {
            state.positionItems = payload
        },
        SET_GENDER_ITEMS: (state, payload) => {
            state.genderItems = payload
        },
        SET_LEAVE_TYPE_ITEMS: (state, payload) => {
            state.leaveTypeItems = payload
        },
        SET_LEAVE_STATUS_ITEMS: (state, payload) => {
            state.leaveStatusItems = payload
        },
        SET_TRAINING_COURSE_TYPE_ITEMS: (state, payload) => {
            state.trainingCourseTypeItems = payload
        },
        SET_PERIOD_TYPE_ITEMS: (state, payload) => {
            state.periodTypeItems = payload
        },
        SET_ROLE_TYPE_ITEMS: (state, payload) => {
            state.roleTypeItems = payload
        },
        SET_COURSE_GROUP_ITEMS: (state, payload) => {
            state.courseGroupItems = payload
        },
        SET_COURSE_ROLE_ITEMS: (state, payload) => {
            state.courseGroupItems = payload
        },
        SET_ANUALLY_ITEMS: (state, payload) => {
            state.annuallyItems = payload
        },
    },
    actions: {
        FIND_PROVINCE: ({commit, dispatch}) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findProvince().then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_PROVINCE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_PROVINCE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_PROVINCE_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_DISTRICT: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findDistrict(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_DISTRICT_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_DISTRICT_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_DISTRICT_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_SUB_DISTRICT: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findSubDistrict(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_SUB_DISTRICT_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_SUB_DISTRICT_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_SUB_DISTRICT_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_DEPARTMENT: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findDepartment(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_DEPARTMENT_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_DEPARTMENT_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_DEPARTMENT_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_CONTRACTS_TYPE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findContractsType(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_CONTRACTS_TYPE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_CONTRACTS_TYPE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_CONTRACTS_TYPE_ITEMS', [])
                    reject()
                })
            })
        },
        GET_All_CONTRACT_TYPE: ({commit, dispatch}) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.getAllContactType().then(v => {
                    const { StatusCode, Errors, Data } = v
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_PERSON_CONTRACTS_TYPE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_PERSON_CONTRACTS_TYPE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_CONTRACTS_TYPE_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_NATIONALITY: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findNationality(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_NATIONALITY_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_NATIONALITY_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_NATIONALITY_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_POSITION: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findPosition(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_POSITION_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_POSITION_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_POSITION_ITEMS', [])
                    reject()
                })
            })
        },
        GET_ALL_GENDER: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.getAllGender(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_GENDER_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_GENDER_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_GENDER_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_LEAVE_TYPE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findLeaveType(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_LEAVE_TYPE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_LEAVE_TYPE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_LEAVE_TYPE_ITEMS', [])
                    reject()
                })
            })
        },
        GET_ALL_LEAVE_STATUS: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.getAllLeaveStatus(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_LEAVE_STATUS_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_LEAVE_STATUS_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_LEAVE_STATUS_ITEMS', [])
                    reject()
                })
            })
        },
        GET_ALL_TRAINING_COURSE_TYPE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.getAllTrainingCourseType(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_TRAINING_COURSE_TYPE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_TRAINING_COURSE_TYPE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_TRAINING_COURSE_TYPE_ITEMS', [])
                    reject()
                })
            })
        },
        GET_ALL_PERIOD_TYPE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.getAllPeriodType(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_PERIOD_TYPE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_PERIOD_TYPE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_PERIOD_TYPE_ITEMS', [])
                    reject()
                })
            })
        },
        GET_ALL_ROLE_TYPE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.getAllRoleType(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_ROLE_TYPE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_ROLE_TYPE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_ROLE_TYPE_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_COURSE_GROUP: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findCourseGroup(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_COURSE_GROUP_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_COURSE_GROUP_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_COURSE_GROUP_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_COURSE_ROLE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findCourseRole(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    console.log('FIND_COURSE_ROLE', v)
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_COURSE_ROLE_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_COURSE_ROLE_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_COURSE_ROLE_ITEMS', [])
                    reject()
                })
            })
        },
        FIND_ANUALLY: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                masterDataService.findAnnually(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    console.log('FIND_ANUALLY', v)
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_ANUALLY_ITEMS', Data.Value.Items)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_ANUALLY_ITEMS', [])
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        resolve([])
                    }

                }).catch(error => {
                    commit('SET_ANUALLY_ITEMS', [])
                    reject()
                })
            })
        }
    }
}

export default admin