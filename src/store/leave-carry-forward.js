import leaveCarryForwardProvider from '@/resources/leave-carry-forward-provider'
const leaveCarryForwardService = new leaveCarryForwardProvider()

const leave = {
    namespaced: true,
    state: {
        criteria: {
            Criteria: {
                Id: null,
                No: null,
                Status: null,
                AnnualId: null,
                Annual: null,
                LeaveTypeId: null
            },
            SortingCriteria: [
                {
                    Name: null,
                    Direction: null
                }
            ],
            PagingCriteria: {
                PageIndex: 0,
                PageSize: 10
            }
        },
        id: null,
        item: null,
        items: [],
        totalRecords: 0,
        totalPages: 0,
        isLoading: false
    },
    mutations: {
        SET_CRITERIA: (state, payload) => {
            state.criteria = payload
        },
        SET_ITEM: (state, payload) => {
            state.item = payload
        },
        SET_ITEMS: (state, payload) => {
            state.items = payload
        },
        SET_ID: (state, payload) => {
            state.id = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_TOTAL_PAGES: (state, payload) => {
            state.totalPages = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        GENERATE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.generate(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        reject(Errors[0].Message)
                    }
                }).catch(error => {
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },

        DRAFT: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                console.log('DRAFT payload', payload)
                leaveCarryForwardService.draft(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    console.log('DRAFT ', v)
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        commit('SET_ID', Data.Value)
                        resolve(Data.Value)
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        commit('SET_ID', null)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    commit('SET_ID', null)
                    reject(error)
                })
            })
        }, 
        APPROVE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.approve(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        commit('SET_ID', Data.Value)
                        resolve(Data.Value)
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('SET_ID', null)
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    commit('SET_ID', null)
                    reject(error)
                })
            })
        }, 
        // VALIDATE: ({commit, dispatch}, payload) => {
        //     return new Promise((resolve, reject) => { 
        //         commit('SET_IS_LOADING', true)
        //         leaveService.validate(payload).then(v => {
        //             const { StatusCode, Errors, Data } = v

        //             if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
        //                 commit('SET_IS_LOADING', false)
        //                 resolve(Data.Value)
        //             } else {
        //                 commit('SET_IS_LOADING', false)
        //                 // commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
        //                 reject(Errors[0].Message)
        //             }

        //         }).catch(error => {
        //             commit('app/SET_ERROR_MESSAGE', error, { root: true })
        //             commit('SET_IS_LOADING', false)
        //             reject(error)
        //         })
        //     })
        // }, 
        FIND: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.find(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_ITEMS', Data.Value.Items)
                        commit('SET_TOTAL_PAGES', Data.Value.TotalPage)
                        commit('SET_IS_LOADING', false)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_ITEMS', [])
                        commit('SET_TOTAL_PAGES', 0)
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('SET_ITEMS', [])
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        }, 
        // FIND_ITEM: ({commit, dispatch}, payload) => {
        //     return new Promise((resolve, reject) => { 
        //         commit('SET_IS_LOADING', true)
        //         leaveService.findItem(payload).then(v => {
        //             const { StatusCode, Errors, Data } = v

        //             if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
        //                 commit('SET_IS_LOADING', false)
        //                 resolve(Data.Value.Items[0])
        //             } else {
        //                 commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
        //                 commit('SET_IS_LOADING', false)
        //                 reject(Errors[0].Message)
        //             }

        //         }).catch(error => {
        //             commit('app/SET_ERROR_MESSAGE', error, { root: true })
        //             commit('SET_IS_LOADING', false)
        //             reject(error)
        //         })
        //     })
        // },
        GET: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.getInfo(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        if (Data.Value) {
                            commit('SET_IS_LOADING', false)
                            resolve(Data.Value)
                        } else {
                            commit('SET_IS_LOADING', false)
                            commit('app/SET_ERROR_MESSAGE', 'Data not found', { root: true })
                            reject([])
                        }
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },
        CANCEL: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.cancel(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('SET_ITEM', {})
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },
        REMOVE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.remove(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('SET_ITEM', {})
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },
        ROLLBACK: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                leaveCarryForwardService.rollback(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('SET_ITEM', {})
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },
        // GET_EMPLOYEE_RIGHTS: ({commit, dispatch}, payload) => {
        //     return new Promise((resolve, reject) => { 
        //         commit('SET_IS_LOADING', true)
        //         leaveService.getEmployeeRights(payload).then(v => {
        //             const { StatusCode, Errors, Data } = v

        //             if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
        //                 commit('SET_IS_LOADING', false)
        //                 resolve(Data.Value.Items)
        //             } else {
        //                 // commit('SET_DEPARTMENT_ITEMS', [])
        //                 commit('SET_IS_LOADING', false)
        //                 resolve([])
        //             }

        //         }).catch(error => {
        //             commit('SET_IS_LOADING', false)
        //             // commit('SET_DEPARTMENT_ITEMS', [])
        //             reject(error)
        //         })
        //     })
        // }
    }

}

export default leave