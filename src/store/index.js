import Vue from 'vue'
import Vuex from 'vuex'
import admin from './admin'
import app from './app'
import commonInformation from './common-information'
import employee from './employee'
import error from './error'
import leave from './leave'
import leaveCarryForward from './leave-carry-forward'
import masterData from './master-data'
import report from './report'
import trainingCourse from './training-course'
import trainingCourseSchedule from './training-course-schedule'
import trainingNotification from './training-notification'

Vue.use(Vuex)

const modules = {
    admin: admin,
    app: app,
    commonInformation: commonInformation,
    employee: employee,
    error: error,
    leave: leave,
    leaveCarryForward: leaveCarryForward,
    masterData: masterData,
    report: report,
    trainingCourse: trainingCourse,
    trainingCourseSchedule: trainingCourseSchedule,
    trainingNotification: trainingNotification
}

export default new Vuex.Store({
    modules
})
