// import { cancel, find, findItem, get, save, validate } from '@/resources/leave-provider'
import TrainingCourseProvider from '@/resources/training-course-provider'
const trainingCourseService = new TrainingCourseProvider()

const leave = {
    namespaced: true,
    state: {
        criteria: {
            Criteria: {
                Code: null,
                Name: null,
                Id: null,
                DateFrom: null,
                DateTo: null,
                AutoCompleteText: null
            },
            SortingCriteria: [
                {
                    Name: null,
                    Direction: 0
                }
            ],
            PagingCriteria: {
                PageIndex: 0,
                PageSize: 0
            }
        },
        item: null,
        items: [],
        totalRecords: 0,
        totalPages: 0,
        isLoading: false
    },
    mutations: {
        SET_CRITERIA: (state, payload) => {
            state.criteria = payload
        },
        SET_ITEM: (state, payload) => {
            state.item = payload
        },
        SET_ITEMS: (state, payload) => {
            state.items = payload
        },
        SET_TOTAL_RECORDS: (state, payload) => {
            state.totalRecords = payload
        },
        SET_TOTAL_PAGES: (state, payload) => {
            state.totalPages = payload
        },
        SET_IS_LOADING: (state, payload) => {
            state.isLoading = payload
        }
    },
    actions: {
        SAVE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                console.log('payload', payload)
                trainingCourseService.save(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    console.log('v', v)
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('SET_IS_LOADING', false)
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        }, 
        FIND: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                trainingCourseService.find(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    console.log('irm', Data.Value.Items)
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_ITEMS', Data.Value.Items)
                        commit('SET_TOTAL_PAGES', Data.Value.TotalPage)
                        commit('SET_IS_LOADING', false)
                        resolve(Data.Value.Items)
                    } else {
                        commit('SET_ITEMS', [])
                        commit('SET_TOTAL_PAGES', 0)
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('SET_ITEMS', [])
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },
        GET: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                trainingCourseService.getInfo(payload).then(v => {
                    const { StatusCode, Errors, Data } = v
                    
                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        if (Data.Value) {
                            commit('SET_IS_LOADING', false)
                            commit('SET_ITEM', Data.Value)
                            resolve(Data.Value)
                        } else {
                            commit('SET_IS_LOADING', false)
                            commit('app/SET_ERROR_MESSAGE', 'Data not found', { root: true })
                            reject([])
                        }
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        },
        REMOVE: ({commit, dispatch}, payload) => {
            return new Promise((resolve, reject) => { 
                commit('SET_IS_LOADING', true)
                trainingCourseService.remove(payload).then(v => {
                    const { StatusCode, Errors, Data } = v

                    if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
                        commit('SET_IS_LOADING', false)
                        resolve()
                    } else {
                        commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
                        commit('SET_IS_LOADING', false)
                        reject(Errors[0].Message)
                    }

                }).catch(error => {
                    commit('SET_ITEM', null)
                    commit('app/SET_ERROR_MESSAGE', error, { root: true })
                    commit('SET_IS_LOADING', false)
                    reject(error)
                })
            })
        }
    }

}

export default leave