
const app = {
    namespaced: true,
    state: {
        message: null
    },
    mutations: {
        SET_ERROR (state, payload) {
            state.message = payload
        }
    },
    actions: {
        
    },
    getters: {

    }
}

export default app