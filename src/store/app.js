
const app = {
    namespaced: true,
    state: {
        barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
        barImage: 'https://demos.creative-tim.com/material-dashboard-pro/assets/img/sidebar-1.jpg',
        drawer: null,
        errorMessage: null,
        loading: false
    },
    mutations: {
        SET_BAR_IMAGE (state, payload) {
            state.barImage = payload
        },
        SET_DRAWER (state, payload) {
            state.drawer = payload
        },
        SET_SCRIM (state, payload) {
            state.barColor = payload
        },
        SET_ERROR_MESSAGE (state, payload) {
            state.errorMessage = payload
        },
        SET_LOADING (state, payload) {
            state.loading = payload
        }
    },
    actions: {
        
    },
    getters: {

    }
}

export default app