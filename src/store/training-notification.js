// import { cancel, find, findItem, get, save, validate } from '@/resources/leave-provider'
import TrainingNotificationProvider from '@/resources/training-notification-provider'
const trainingNotificationService = new TrainingNotificationProvider()

const leave = {
  namespaced: true,
  state: {
    criteria: {
      Criteria: {
        Code: null,
        Name: null,
        Id: null,
        DateFrom: null,
        DateTo: null,
        AutoCompleteText: null
      },
      SortingCriteria: [
        {
          Name: null,
          Direction: 0
        }
      ],
      PagingCriteria: {
        PageIndex: 0,
        PageSize: 0
      }
    },
    item: null,
    presentMonthItems: [],
    nextMonthItems: [],
    courseItems: [],
    employeeItems: [],
    totalRecords: 0,
    totalPages: 0,
    isLoading: false,
    isCourseLoading:false,
    isEmployeeLoading:false
  },
  mutations: {
    SET_CRITERIA: (state, payload) => {
      state.criteria = payload
    },
    SET_ITEM: (state, payload) => {
      state.item = payload
    },
    SET_PESENT_MONTH_ITEMS: (state, payload) => {
      state.presentMonthItems = payload
    },
    SET_NEXT_MONTH_ITEMS: (state, payload) => {
      state.nextMonthItems = payload
    },
    SET_COURSE_ITEMS: (state, payload) => {
      state.courseItems = payload
    },
    SET_EMPLOYEE_ITEMS: (state, payload) => {
      state.employeeItems = payload
    },
    SET_ITEMS: (state, payload) => {
      state.items = payload
    },
    SET_TOTAL_RECORDS: (state, payload) => {
      state.totalRecords = payload
    },
    SET_TOTAL_PAGES: (state, payload) => {
      state.totalPages = payload
    },
    SET_IS_LOADING: (state, payload) => {
      state.isLoading = payload
    },
    SET_IS_COURSE_LOADING: (state, payload) => {
      state.isCourseLoading = payload
    },
    SET_IS_EMPLOYEE_LOADING: (state, payload) => {
      state.isEmployeeLoading = payload
    }
  },
  actions: {
    GET_CURRENR_TRAINING_PLAN: ({commit, dispatch}, payload) => {
      return new Promise((resolve, reject) => { 
        commit('SET_IS_LOADING', true)
        trainingNotificationService.getCurrentTrainingPlan(payload).then(v => {
          const { StatusCode, Errors, Data } = v
          console.log('result', v)
          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            if (Data.Value) {
              commit('SET_IS_LOADING', false)
              commit('SET_PESENT_MONTH_ITEMS', Data.Value.PresentMonthItems)
              commit('SET_NEXT_MONTH_ITEMS', Data.Value.NextMonthItems)
              resolve(Data.Value)
            } else {
              commit('SET_IS_LOADING', false)
              commit('app/SET_ERROR_MESSAGE', 'Data not found', { root: true })
              reject([])
            }
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    GET_TRAINING_PLAN: ({commit, dispatch}, payload) => {
      return new Promise((resolve, reject) => { 
        commit('SET_IS_LOADING', true)
        trainingNotificationService.GetTrainingPlan(payload).then(v => {
          const { StatusCode, Errors, Data } = v
          console.log('result', v)
          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            if (Data.Value) {
              commit('SET_IS_LOADING', false)
              commit('SET_NEXT_MONTH_ITEMS', Data.Value.NextMonthItems)
              resolve(Data.Value)
            } else {
              commit('SET_IS_LOADING', false)
              commit('app/SET_ERROR_MESSAGE', 'Data not found', { root: true })
              reject([])
            }
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_LOADING', false)
            reject(Errors[0].Message)
          }

        }).catch(error => {
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_LOADING', false)
          reject(error)
        })
      })
    },
    GET_COURSE: ({commit, dispatch}, payload) => {
      return new Promise((resolve, reject) => { 
        commit('SET_IS_COURSE_LOADING', true)
        trainingNotificationService.getCourses(payload).then(v => {
          const { StatusCode, Errors, Data } = v

          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            if (Data.Value) {
              commit('SET_IS_COURSE_LOADING', false)
              commit('SET_COURSE_ITEMS', Data.Value)
              resolve(Data.Value)
            } else {
              commit('SET_IS_COURSE_LOADING', false)
              commit('app/SET_ERROR_MESSAGE', 'Data not found', { root: true })
              reject([])
            }
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_COURSE_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_COURSE_LOADING', false)
          reject(error)
        })
      })
    },
    GET_EMPLOYEES: ({commit, dispatch}, payload) => {
      return new Promise((resolve, reject) => { 
        commit('SET_IS_EMPLOYEE_LOADING', true)
        trainingNotificationService.getEmployees(payload).then(v => {
          const { StatusCode, Errors, Data } = v
          if (StatusCode.toString() === process.env.VUE_APP_API_STATUS_SUCCESS) {
            if (Data.Value) {
              commit('SET_IS_EMPLOYEE_LOADING', false)
              commit('SET_EMPLOYEE_ITEMS', Data.Value.Items)
              resolve(Data.Value)
            } else {
              commit('SET_IS_EMPLOYEE_LOADING', false)
              commit('app/SET_ERROR_MESSAGE', 'Data not found', { root: true })
              reject([])
            }
          } else {
            commit('app/SET_ERROR_MESSAGE', Errors[0].Message, { root: true })
            commit('SET_IS_EMPLOYEE_LOADING', false)
            reject(Errors[0].Message)
          }
        }).catch(error => {
          commit('app/SET_ERROR_MESSAGE', error, { root: true })
          commit('SET_IS_EMPLOYEE_LOADING', false)
          reject(error)
        })
      })
    }
  }
}

export default leave