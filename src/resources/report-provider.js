// import api from './api'
import HttpRequest from './http-request'

class ReportProvider extends HttpRequest {
  constructor() {
    super()
  }

  //#region EmployeeReport
  ageRange = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('HrmModule/EmployeeReport/AgeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  ageRangeExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/RenderAgeGroupingReport', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  yearExperience = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('HrmModule/EmployeeReport/WorkingYearGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }


  yearExperienceExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/RenderWorkingYearGroupingReport', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  turnOverRate = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/FindTurnOverAgeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  turnOverExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/RenderTurnOverReport', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  findTurnOverDetail = (payload) => {
    return new Promise((resolve, reject) => {
      this.create(`/HrmModule/EmployeeReport/FindTurnOverDetail`, payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  employee = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/FindEmployeeContractTypeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  employeeExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/RenderEmployeeContractTypeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  resignationEmployee = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/FindResignContractTypeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  resignationEmployeeExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/EmployeeReport/RenderResignContractTypeGroupingReport', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }
  //#endregion EmployeeReport

  //#region LeaveReport
  leaveHistoryTypeGrouping = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/LeaveReport/GetLeaveHistoryTypeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  leaveHistoryTypeGroupingExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/LeaveReport/RenderLeaveHistoryTypeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  leaveBalance = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/LeaveReport/FindLeaveBalance', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }
  //#endregion LeaveReport

  //#region TriningReport
  trainingHistory = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/TrainingPlan/FindTrainingHistory', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  trainingHistoryInfo = (payload) => {
    return new Promise((resolve, reject) => {
      this.get(`/HrmModule/TrainingPlan/GetTrainingHistory?employeeId=${payload.employeeId}&courseId=${payload.courseId}`).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  trainingAgeGrouping = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/TrainingReport/GetHistoryAgeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  trainingAgeGroupingExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/TrainingReport/RenderTrainingAgeGrouping', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  trainingSummary = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/TrainingReport/GetTrainingSummary', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  trainingSummaryExport = (payload) => {
    return new Promise((resolve, reject) => {
      this.create('/HrmModule/TrainingReport/RenderTrainingSummary', payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }
  //#endregion TriningReport


}

export default ReportProvider
