// import api from './api'
import HttpRequest from './http-request'

class AdminProvider extends HttpRequest {
    constructor () {
        super()
    }

    authLogin = (payload) => {
        return new Promise((resolve, reject) => {
            this.request('POST', '/CoreModule/FormAuthen/Login', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default AdminProvider
