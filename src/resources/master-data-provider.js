// import api from './api'
import HttpRequest from './http-request'

class MasterDataProvider extends HttpRequest {
    constructor () {
        super()
    }

    findProvince = () => {
        return new Promise((resolve, reject) => {
            this.get('/ELModule/AddressLocation/FindProvinces').then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findDistrict = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/ELModule/AddressLocation/FindAllDistricts?provinceId=${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findSubDistrict = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/ELModule/AddressLocation/FindAllSubDistricts?districtId=${payload}`).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findDepartment = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/FindDepartment', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findContractsType = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/ContractsType/FindContractsType', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getAllContactType = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Employee/GetAllContactType', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    findNationality = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/FindNationality', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findPosition = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/FindPosition', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getAllGender = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/GetAllGender', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findLeaveType = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveType/FindLeaveType', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getAllLeaveStatus = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveRequest/GetAllDocStatus', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getAllTrainingCourseType = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/TrainingCourse/GetAllTrainingCourseType', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getAllPeriodType = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/TrainingCourse/GetAllPeriodType', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getAllRoleType = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/GetAllRoleType', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findCourseGroup = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/FindCourseGroup', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findCourseRole = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/FindCourseRole', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findAnnually = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Annually/FindAnnually', payload).then(v => {
                resolve(v.data) 
            }).catch((error) => {
                reject(error)
            })
        })
    }

    

}

export default MasterDataProvider