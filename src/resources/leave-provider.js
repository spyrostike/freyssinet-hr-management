// import api from './api'
import HttpRequest from './http-request'

class LeaveProvider extends HttpRequest {
    constructor () {
        super()
    }

    draft = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveRequest/Save', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    approve = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveRequest/Approve', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    validate = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveRequest/RightsVerifier', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    find = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveRequest/Find', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findItem = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveRequest/FindItem', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getInfo = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveRequest/Get?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    cancel = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveRequest/Cancel?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    remove = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveRequest/Remove?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    rollback = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveRequest/Rollback?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    cancel = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveRequest/Cancel?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getEmployeeRights = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveRequest/GetEmployeeRights?employeeId=${payload.id}&leaveDateFrom=${payload.dateFrom}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default LeaveProvider
