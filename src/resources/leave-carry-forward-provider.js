import HttpRequest from './http-request'

class LeaveCarryForwardProvider extends HttpRequest {
    constructor () {
        super()
    }

    draft = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveCarryForward/Save', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    approve = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveCarryForward/Approve', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    rollback = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveCarryForward/Rollback?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    cancel = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveCarryForward/Cancel?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    generate = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveCarryForward/GenerateLeaveCFCFEmployee?leaveTypeId=${payload.leaveTypeId}&annual=${payload.annual}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    find = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/LeaveCarryForward/Find', payload).then(v => {
                console.log('data', v.data)
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getInfo = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveCarryForward/Get?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    remove = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/LeaveCarryForward/Remove?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default LeaveCarryForwardProvider