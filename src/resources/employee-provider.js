// import api from './api'
import HttpRequest from './http-request'

class EmployeeProvider extends HttpRequest {
    constructor () {
        super()
    }

    save = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Employee/Save', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    find = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Employee/Find', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    employeeExport = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Employee/RenderEmployee', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findLeader = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Employee/FindLeader', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findForeman = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Employee/FindForeman', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    getInfo = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/Employee/Get?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    resign = (payload) => {
        return new Promise((resolve, reject) => {
            this.create(`/HrmModule/Employee/Resign`, payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findLeaveHistory = (payload) => {
        return new Promise((resolve, reject) => {
            this.create(`/HrmModule/LeaveReport/FindLeaveHistory`, payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    findTrainingHistory = (payload) => {
        return new Promise((resolve, reject) => {
            this.create(`/HrmModule/TrainingReport/FindEmployeeTraining`, payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default EmployeeProvider
