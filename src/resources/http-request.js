import axios from 'axios'
// import { clientGUID } from '../utils/client'
// import { APP_ID, API_ENDPOINT } from '../app-config'
import store from '../store'
import router from '../router'

class HttpRequest {
    constructor (url = process.env.VUE_APP_API_ENDPOINT) {
        // this.axios = axios
        
        this.axiosInstance = axios.create({
            baseURL: url,
            timeout: 180000,
            headers: { 'Content-Type': 'application/json', 'accessId': process.env.VUE_APP_APPLICATION_ID },
            accessId: process.env.VUE_APP_APPLICATION_ID,
        })

        this.axiosInstance.interceptors.request.use(function (config) {
            // Do something before request is sent

            const token = store.state.admin.token
            
            if (token) { config.headers.common['authId'] = token }

            return config
        }, function (error) {
            // Do something with request error
            return Promise.reject(error)
        })

        // Add a response interceptor
        this.axiosInstance.interceptors.response.use(async function (response) {
            // Do something with response data
            const { Errors, StatusCode  } = response.data
            console.log('StatusCode', StatusCode, process.env.VUE_APP_API_UNAUTOLIZED)
            if (StatusCode.toString() === process.env.VUE_APP_API_UNAUTOLIZED) {
                
                localStorage.removeItem(process.env.VUE_APP_USER_TOKEN_NAME)
                await store.dispatch('admin/AUTH_LOGOUT')
                router.push({ path: '/admin/login' })
            }

            return response
        }, function (error) {
            // Do something with response error
            
            // if (error.response.status === 401) {
            //     store.dispatch('admin/AUTH_LOGOUT').then(() => {
            //         router.push('/admin/login')
            //     })
            // }
            return Promise.reject(error)
        })
    }

    setHeader (key, value) {
        // this.axiosInstance.defaults.headers.common[header.key] = header.value
        // this.axiosInstance.defaults.headers.common = header
        
        this.axiosInstance.defaults.headers.common[key] = value

        // this.axiosInstance.defaults.headers.post['Content-Type'] = 'application/json'

        // const { authId } = header

        // if (authId) {
        //   this.axiosInstance.defaults.headers.common['authId'] = authId
        // }

        // this.axiosInstance.defaults.headers.common['accessId'] = APP_ID
    
    }

    removeAllHeaders = () => {
        delete instance.defaults.headers.common['authId']
        delete instance.defaults.headers.common['accessId']
    }

    get (methodName, data) {
            return this.axiosInstance.get(methodName, {
            params: data
        })
    }

    create (methodName, data) {
        return this.axiosInstance.post(methodName, data)
    }

    update (methodName, data) {
        return this.axiosInstance.put(methodName, data)
    }

    delete (methodName, param, data) {
        return this.axiosInstance.delete(methodName, {
            params: param,
            data: data
        })
    }

    request (type, url, data) {
        let promise = null
        axios.defaults.baseURL = process.env.VUE_APP_API_ENDPOINT

        // axios.defaults.headers.common['regsToken'] = `${clientGUID()}`
        axios.defaults.headers.common['accessId'] = process.env.VUE_APP_APPLICATION_ID

        switch (type) {
            case 'GET': promise = axios.get(url, { params: data }); break
            case 'POST': promise = axios.post(url, data); break
            case 'PUT': promise = axios.put(url, data); break
            case 'DELETE': promise = axios.delete(url, data); break
            default : promise = axios.get(url, { params: data }); break
        }
        return promise
    }
}

export default HttpRequest