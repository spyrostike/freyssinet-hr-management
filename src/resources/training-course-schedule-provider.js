// import api from './api'
import HttpRequest from './http-request'

class trainingCourseProvider extends HttpRequest {
    constructor () {
        super()
    }

    generateSchedule = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/Scheduler/genarateScheduler?dateStartFrom=${payload.dateFrom}&dateStartTo=${payload.dateTo}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                console.log('sdsdg error', error)
                reject(error)
            })
        })
    }

    save = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Scheduler/Save', payload).then(v => {
                console.log('payload', payload)
                resolve(v.data)
            }).catch((error) => {
                console.log('sdsdg error', error)
                reject(error)
            })
        })
    }

    find = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Scheduler/Find', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getInfo = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/Scheduler/Get?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    duplicate = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/Scheduler/Duplicate?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    saveSignRecord = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/Scheduler/SaveSignRecord', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    remove = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/Scheduler/Remove?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default trainingCourseProvider
