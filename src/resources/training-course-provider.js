// import api from './api'
import HttpRequest from './http-request'

class trainingCourseProvider extends HttpRequest {
    constructor () {
        super()
    }

    save = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/TrainingCourse/Save', payload).then(v => {
                console.log('payload', payload)
                resolve(v.data)
            }).catch((error) => {
                console.log('sdsdg error', error)
                reject(error)
            })
        })
    }

    find = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/HrmModule/TrainingCourse/Find', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    getInfo = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/TrainingCourse/Get?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    remove = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/HrmModule/TrainingCourse/Remove?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

export default trainingCourseProvider
