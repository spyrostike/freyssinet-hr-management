// import api from './api'
import HttpRequest from './http-request'

class Provider extends HttpRequest {
    constructor () {
        super()
    }

    save = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/SaveRole', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    find = (payload) => {
        return new Promise((resolve, reject) => {
            this.create('/ELModule/Role/FindRole', payload).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    
    getInfo = (payload) => {
        return new Promise((resolve, reject) => {
            this.get(`/ELModule/Role/GetRole?id=${payload}`).then(v => {
                resolve(v.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

export default Provider
