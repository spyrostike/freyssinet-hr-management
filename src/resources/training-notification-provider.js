// import api from './api'
import HttpRequest from './http-request'

class ReportProvider extends HttpRequest {
  constructor () {
    super()
  }

  getCurrentTrainingPlan = (payload) => {
    return new Promise((resolve, reject) => {
      this.create(`HrmModule/TrainingPlan/GetCurrentTrainingPlan`).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  GetTrainingPlan = (payload) => {
    return new Promise((resolve, reject) => {
      this.get(`/HrmModule/TrainingPlan/GetTrainingPlan?criteria=${payload}`).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  getCourses = (payload) => {
    return new Promise((resolve, reject) => {
      this.get(`/HrmModule/TrainingPlan/GetCourseGroupingPlan?dueDate=${payload}`).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  getEmployees = (payload) => {
    return new Promise((resolve, reject) => {
      this.create(`/HrmModule/TrainingPlan/FindTrainingPlanDetail`, payload).then(v => {
        resolve(v.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }
}

export default ReportProvider
