import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters['admin/isAuthenticated']) {
        next()
        return
    }

    next('/admin')
}

const ifAuthenticated = async (to, from, next) => {

    if (store.getters['admin/isAuthenticated']) {
        next()
        return
    }
    next('/admin/login')
}

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/admin/login',
            component: () => import('@/views/admin/form/Login'),
            name: 'admin-login',
            beforeEnter: ifNotAuthenticated
        },
        {
            path: '/admin',
            name: 'dashboard',
            component: () => import('@/layouts/admin/Index'),
            meta: { title: 'home' },
            beforeEnter: ifAuthenticated,
            children: [
                { path: '/', redirect: { name: 'admin-dashboard' } },
                { name: 'admin-dashboard', path: 'dashboard', component: () => import('@/views/admin/dashboard/Index'), meta: { title: 'dashboard' } },
                { name: 'admin-training-course-schedule', path: 'training-course-schedule', component: () => import('@/views/admin/training-course-schedule/Index'), meta: { title: 'training-course-schedule' } },
                { name: 'admin-common-information', path: 'common-information', component: () => import('@/views/admin/common-information/Index'), meta: { title: 'common-information' } },
                {
                    name: 'admin-common-information',
                    path: 'common-information',
                    component: () => import('@/router/template/EmptyRouterView.vue'),
                    meta: { title: 'common-information' },
                    children: [
                        { name: 'admin-common-information-add', path: 'add', component: () => import('@/views/admin/common-information/Info'), meta: { title: 'common-information', subtitle: 'add' } },
                        { name: 'admin-common-information-edit', path: 'edit/:id', component: () => import('@/views/admin/common-information/Info'), meta: { title: 'common-information', subtitle: 'edit' } }
                    ]
                },
                { name: 'admin-employee', path: 'employee', component: () => import('@/views/admin/employee/Index'), meta: { title: 'employee' } },
                {
                    name: 'admin-employee',
                    path: 'employee',
                    component: () => import('@/router/template/EmptyRouterView.vue'),
                    meta: { title: 'employee' },
                    children: [
                        { name: 'admin-employee-add', path: 'add', component: () => import('@/views/admin/employee/Info'), meta: { title: 'employee', subtitle: 'add' } },
                        { name: 'admin-employee-edit', path: 'edit/:id', component: () => import('@/views/admin/employee/Info'), meta: { title: 'employee', subtitle: 'edit' } },
                    ]
                },
                { name: 'admin-leave', path: 'leave', component: () => import('@/views/admin/leave/Index'), meta: { title: 'leave' } },
                {
                    name: 'admin-leave',
                    path: 'leave',
                    component: () => import('@/router/template/EmptyRouterView.vue'),
                    meta: { title: 'leave' },
                    children: [
                        { name: 'admin-leave-request-add', path: 'request', component: () => import('@/views/admin/leave/Request'), meta: { title: 'leave-request' } },
                        { name: 'admin-leave-request-edit', path: 'request/:id', component: () => import('@/views/admin/leave/Request'), meta: { title: 'leave-request' } }
                    ]
                },
                { name: 'admin-leave-carry-forward', path: 'leave-carry-forward', component: () => import('@/views/admin/leave-carry-forward/Index'), meta: { title: 'leave-carry-forward' } },
                {
                    name: 'admin-leave-carry-forward',
                    path: 'leave-carry-forward',
                    component: () => import('@/router/template/EmptyRouterView.vue'),
                    meta: { title: 'leave-carry-forward' },
                    children: [
                        { name: 'admin-leave-carry-forward-request-add', path: 'request', component: () => import('@/views/admin/leave-carry-forward/Request'), meta: { title: 'leave-carry-forward-request' } },
                        { name: 'admin-leave-carry-forward-request-edit', path: 'request/:id', component: () => import('@/views/admin/leave-carry-forward/Request'), meta: { title: 'leave-carry-forward-request' } },
                    ]
                },
                { name: 'admin-training-course', path: 'training-course', component: () => import('@/views/admin/training-course/Index'), meta: { title: 'training-course' } },
                {
                    name: 'admin-training-course',
                    path: 'training-course',
                    component: () => import('@/router/template/EmptyRouterView.vue'),
                    meta: { title: 'training-course' },
                    children: [
                        { name: 'admin-training-course-add', path: 'add', component: () => import('@/views/admin/training-course/Info'), meta: { title: 'training-course', subtitle: 'add' } },
                        { name: 'admin-training-course-edit', path: 'edit/:id', component: () => import('@/views/admin/training-course/Info'), meta: { title: 'training-course', subtitle: 'edit' } }
                    ]
                },
                { name: 'admin-training-course-schedule', path: 'training-course-schedule', component: () => import('@/views/admin/training-course-schedule/Index'), meta: { title: 'training-course-schedule' } },
                {
                    name: 'admin-training-course-schedule',
                    path: 'training-course-schedule',
                    component: () => import('@/router/template/EmptyRouterView.vue'),
                    meta: { title: 'training-course-schedule' },
                    children: [
                        { name: 'admin-training-course-schedule-add', path: 'add', component: () => import('@/views/admin/training-course-schedule/Info'), meta: { title: 'training-course-schedule', subtitle: 'add' } },
                        { name: 'admin-training-course-schedule-edit', path: 'edit/:id', component: () => import('@/views/admin/training-course-schedule/Info'), meta: { title: 'training-course-schedule', subtitle: 'edit' } },
                        { name: 'admin-training-course-schedule-duplicate', path: 'duplicate/:id', component: () => import('@/views/admin/training-course-schedule/Info'), meta: { title: 'training-course', subtitle: 'duplicate' } },
                        // { name: 'admin-training-course-schedule-attendance-record', path: 'attandance-record/:id', component: () => import('@/views/admin/training-course-schedule/AttendanceRecord'), meta: { title: 'training-course-schedule-attendance-record' } }
                    ]
                },
                { name: 'admin-report-age-range', path: 'report/age-range', component: () => import('@/views/admin/report/age-range/Index'), meta: { title: 'report-age-range' } },
                { name: 'admin-year-experience', path: 'report/year-experience', component: () => import('@/views/admin/report/year-experience/Index'), meta: { title: 'report-year-experience' } },
                { name: 'admin-turn-over-rate', path: 'report/turn-over-rate', component: () => import('@/views/admin/report/turn-over-rate/Index'), meta: { title: 'report-turn-over-rate' } },
                { name: 'admin-employee', path: 'report/employee', component: () => import('@/views/admin/report/employee/Index'), meta: { title: 'report-employee' } },
                { name: 'admin-resignation-employee', path: 'report/resignation-employee', component: () => import('@/views/admin/report/resignation-employee/Index'), meta: { title: 'report-resignation-employee' } },

                { name: 'admin-leave-history-type-grouping', path: 'report/leave-history-type-grouping', component: () => import('@/views/admin/report/leave-history-type-grouping/Index'), meta: { title: 'report-leave-history-type-grouping' } },
                { name: 'admin-leave-balance-employee', path: 'report/leave-balance', component: () => import('@/views/admin/report/leave-balance/Index'), meta: { title: 'report-leave-balance' } },

                { name: 'admin-report-training-history', path: 'report/training-history', component: () => import('@/views/admin/report/training-history/Index'), meta: { title: 'report-training-history' } },
                { name: 'admin-report-training-age-range', path: 'report/training-age-range', component: () => import('@/views/admin/report/training-age-range/Index'), meta: { title: 'report-training-age-range' } },
                { name: 'admin-report-training-summary', path: 'report/training-summary', component: () => import('@/views/admin/report/training-summary/Index'), meta: { title: 'report-training-summary' } },


                { path: '*', redirect: { name: 'admin-dashboard' } }
            ]
        },
        {
            path: '*',
            component: () => import('@/views/pages/Index'),
            children: [
                {
                    name: '404 Error',
                    path: '',
                    component: () => import('@/views/pages/Error')
                }
            ]
        }
    ],
})
